Use your free JIRA projects to practice testing Web Services

Task 1

Create a story in JIRA via JIRA API and Postman

Use Base Auth with using the JIRA API token
Follow the best practicies in creating a story
Set priority based on the severity
Task 2

Create a bug in JIRA via JIRA API and Postman

Use Base Auth with using the JIRA API token
Follow the best practicies in creating a bug
Set priority based on the severity
Task 3

Link the bug to the story in JIRA via JIRA API and Postman

Use Base Auth with using the JIRA API token
Use relation 'is blocked by'


Hints and what don't forget to have:

Meaningful Title

Steps to reproduce

Expected vs. Actual result

Severity/Priority

Prerequisites

Additional info

Classifications 

Others as screenshots, an assignee, transitions and everything useful 
